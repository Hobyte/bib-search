# Blautopf

A search aggregation app for scientific articels. Currently supports  [havard library](https://library.harvard.edu/) and [oapen](https://oapen.org/)

## Run

- run `ng serve`
- open [localhost:4200](localhost:4200) in your browser
- enter a search term to start a search
