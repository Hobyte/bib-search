import { Result } from "../core/result";
import { SearchProvider } from "../core/search-provider";
import { Parser, parseString } from "xml2js";

export class Harvard implements SearchProvider {
  name = "Harvard Library";

  async search(searchTerm: string): Promise<Result[]> {
    var results: Result[] = [];

    var answer = await fetch(`https://corsproxy.io/?https://api.lib.harvard.edu/v2/items.json?q=${searchTerm}&limit=250`)
      .then(response => {return response.json();})
      .then(results => { return results });

    answer.items.mods.forEach((item: any) => {
      const newResult: Result = {
        title: "",
        authors: "",
        description: "",
        source: this.name,
        link: new URL("https://oapen.org/"),
      };

      try {
        newResult.title = item.titleInfo.title;
        if (newResult.title == undefined) {
          throw new Error;
        }
        newResult.link = new URL(item.relatedItem[1].location.url);

        try {
          newResult.description = item.abstract['#text'] == undefined ? " " : item.abstract['#text'];
        } catch (error) { }

        try {
          item.name.forEach((name: any) => {
            newResult.authors.concat(name);
          })
        } catch (error) { }

        results.push(newResult);
      } catch (error) { }
    });

    return new Promise((resolve, reject) => {
      resolve(results);
    });
  }
}