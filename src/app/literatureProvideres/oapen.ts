import { Result } from "../core/result";
import { SearchProvider } from "../core/search-provider";

export class Oapen implements SearchProvider {
  name = "Open Access Publishing in European Networks";

  async search(searchTerm: string): Promise<Result[]> {
    var results: Result[] = [];

    var answer = await fetch(`https://corsproxy.io/?https://library.oapen.org/rest/search?query=${searchTerm}&expand=metadata`, {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    })
      .then(response => {return response.json();})
      .then(results => { return results });

    const newResult: Result = {
      title: "",
      authors: "",
      description: "",
      source: this.name,
      link: new URL("https://oapen.org/"),
    };

    answer.forEach((result: any) => {
      result.metadata.forEach((metadata: any) => {
        switch (metadata.key) {
          case "dc.title":
            newResult.title = metadata.value;
            break;
          case "dc.contributor.author":
            newResult.authors = newResult.authors + " " + metadata.value;
            break;
          case "dc.description.abstract":
            newResult.description = metadata.value;
            break;
          case "dc.identifier.uri":
            newResult.link = new URL(metadata.value);
            break;
        }

        results.push(newResult);
      });
    });

    results = results.slice(0, 250);

    return new Promise((resolve, reject) => {
      resolve(results);
    });
  }
}
