import { Result } from "./result";

export interface SearchProvider{
    name: string;

    search(term: string): Promise<Result[]>;
}
