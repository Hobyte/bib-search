import { Injectable } from "@angular/core";
import { Result } from "./result";
import { Harvard } from "../literatureProvideres/harvard";
import { Oapen } from "../literatureProvideres/oapen";
import { matchSorter } from "match-sorter";

@Injectable({
    providedIn: 'root'
},)
export class Searcher {
    literatureSources = [
        new Oapen(),
        new Harvard(),
    ]
    
    async search(query: string): Promise<Result[]> {
        var combinedResults: Result[] = [];

        for (let index = 0; index < this.literatureSources.length; index++) {
            const literatureSource = this.literatureSources[index];
            combinedResults = combinedResults.concat(await literatureSource.search(query));
        }
        
        combinedResults = combinedResults.flat();
        combinedResults = matchSorter(combinedResults, query, {keys: ['title', 'description']});
        
        return new Promise((resolve, reject) => {
            resolve(combinedResults);
          });
    }

    getSources(): string[] {
        var sourceNames: string[] = [];
        this.literatureSources.forEach(sources => {
            sourceNames.push(sources.name);
        })
        return sourceNames;
    }
}