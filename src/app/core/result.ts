export interface Result {
    title: string;
    authors: string;
    description: string;
    source: string;
    link: URL;
}
