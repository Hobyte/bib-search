export enum SortingField {
    title = "Titel",
    authors = "Autor",
    description = "Beschreibung",
    source = "Quelle",
}