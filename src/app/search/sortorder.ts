export enum SortingOrder {
    ascending = "Aufsteigend",
    descending = "Absteigend",
    matching = "Passend",
}