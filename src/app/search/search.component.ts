import { Component, inject } from '@angular/core';
import { Searcher } from '../core/searcher';
import { Result } from '../core/result';
import { SortingOrder } from './sortorder';
import { SortingField } from './sortingfield';
import { FormControl } from '@angular/forms';
import { matchSorter } from 'match-sorter';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {
  searcher: Searcher = inject(Searcher);
  allResultsList: Result[] = [];
  resultsList: Result[] = [];

  sortingField = SortingField;
  sortingOrder = SortingOrder;

  currentSortingField: string = 'title';
  currentSortingOrder: string = this.sortingOrder.matching;
  currentSearchString: string = '';
  currentSources: string[] = [];

  sortingFieldControler = new FormControl();

  constructor(private spinner: NgxSpinnerService) { }

  async startSearch(searchTerm: string) {
    this.resultsList = [];
    this.spinner.show();
    this.allResultsList = await this.searcher.search(searchTerm);
    this.sortResultList();
    this.currentSearchString = searchTerm;
    this.spinner.hide();
  }

  updateSortingField(value: string) {
    switch (value) {
      case 'Titel': this.currentSortingField = 'title'; break;
      case 'Autor': this.currentSortingField = 'authors'; break;
      case 'Beschreibung': this.currentSortingField = 'description'; break;
      case 'Quelle': this.currentSortingField = 'source'; break;
    }

    this.sortResultList();
  }

  updateWithSortingOrder(value: string) {
    this.currentSortingOrder = value;
    this.sortResultList();
  }

  updateSources(value: string[]) {
    this.currentSources = value;
    this.sortResultList();
  }

  private sortResultList() {
    this.resultsList = this.filterResultsList();
    
    switch (this.currentSortingOrder) {
      case SortingOrder.ascending:
        this.resultsList.sort((first, second) => { return first[this.currentSortingField as keyof Result].toString().localeCompare(second[this.currentSortingField as keyof Result].toString()) });
        break;
      case SortingOrder.descending:
        this.resultsList.sort((first, second) => { return first[this.currentSortingField as keyof Result].toString().localeCompare(second[this.currentSortingField as keyof Result].toString()) }).reverse();
        break;
      case SortingOrder.matching:
        this.resultsList = matchSorter(this.resultsList, this.currentSearchString, { keys: [this.currentSortingField] });
        break;
    }
  }

  private filterResultsList(): Result[] {
    if (this.currentSources.length <= 0) {
      return this.allResultsList;
    }

    var filteredResults: Result[] = [];

    this.currentSources.forEach(source => {
      this.allResultsList.forEach(result => {
        if (source == result.source) {
          filteredResults.push(result);
        }
      });
    });

    return filteredResults;
  }
}
